﻿using IngredientConverter.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IngredientConverter.Business.Converters
{
    /// <summary>
    /// Interface for the converter used to perform all conversions from one measurement to another.
    /// </summary>
    public interface IConverter
    {
        /// <summary>
        /// Standard conversion for measurements of the same type.  Converts the quantity from the source measurement
        /// units and returns it in the destination measurement units.
        /// </summary>
        /// <param name="srcMeasurement">Source measurement.</param>
        /// <param name="destMeasurement">Destination measuremnt.</param>
        /// <param name="quantity">Quantity to convert.</param>
        /// <returns>Converted quantity.</returns>
        double Convert(IMeasurement srcMeasurement, IMeasurement destMeasurement, double quantity);

        /// <summary>
        /// Complex conversion for measurements of different types (mass vs. volume).  This is made possible by
        /// using the given ingredient.  Converts the quantity from the source measurement units and returns it 
        /// in the destination measurement units.
        /// </summary>
        /// <param name="ingredient">Ingredient performing conversion for.</param>
        /// <param name="srcMeasurement">Source measurement.</param>
        /// <param name="destMeasurement">Destination measuremnt.</param>
        /// <param name="quantity">Quantity to convert.</param>
        /// <returns>Converted quantity.</returns>
        double Convert(IIngredient ingredient, IMeasurement srcMeasurement, IMeasurement destMeasurement, double quantity);
    }
}
