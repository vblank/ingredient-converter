﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IngredientConverter.Business.Converters
{
    /// <summary>
    /// Interface for measurement converters.
    /// </summary>
    public interface IMeasurementConverter
    {
        /// <summary>
        /// Converts the given quantity of the measurement to base units (ounces).
        /// </summary>
        /// <param name="quantity">Quantity to convert.</param>
        /// <returns>Number of base units that equal the given quantity for the measurement.</returns>
        double ConvertToBaseUnits(double quantity);

        /// <summary>
        /// Converts the given quantity of base units (ounces) to equal measurement units.
        /// </summary>
        /// <param name="baseUnits">Base units (ounces) to convert.</param>
        /// <returns>Number of measurement units that equal the given quantity of base units.</returns>
        double ConvertFromBaseUnits(double baseUnits);
    }
}