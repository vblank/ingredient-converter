﻿using IngredientConverter.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IngredientConverter.Business.Converters
{
    /// <summary>
    /// MeasurementConverter Factory.
    /// </summary>
    public interface IMeasurementConverterFactory
    {
        /// <summary>
        /// Encapsulate the logic for creating an Measurement converter.  Allows for a single point of change
        /// in the future if a different MeasurementConverter is desired.
        /// </summary>
        /// <param name="Measurement">Measurement type of the converter.</param>
        /// <returns>A new MeasurementConverter.</returns>
        IMeasurementConverter GetMeasurementConverter(IMeasurement measurement);
    }
}
