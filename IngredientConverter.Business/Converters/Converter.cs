﻿using IngredientConverter.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IngredientConverter.Business.Converters
{
    /// <summary>
    /// Converter used to perform all conversions from one measurement to another.
    /// </summary>
    public class Converter : IConverter
    {
        IIngredientConverterFactory _ingredientConverterFactory;
        IMeasurementConverterFactory _measurementConverterFactory;

        /// <summary>
        /// Initialize the converter and its dependencies.
        /// </summary>
        public Converter()
        {
            _ingredientConverterFactory = new IngredientConverterFactory();
            _measurementConverterFactory = new MeasurementConverterFactory();
        }

        /// <summary>
        /// Initialize the converter using the passed in dependencies.  This allows for easier testing and to make future implementation
        /// of dependency injection possible.
        /// </summary>
        /// <param name="ingredientConverterFactory">The factory for creating IngredientConverters.</param>
        /// <param name="measurementConverterFactory">The factory for creating MeasurementConverters.</param>
        public Converter(IIngredientConverterFactory ingredientConverterFactory, IMeasurementConverterFactory measurementConverterFactory)
        {
            _ingredientConverterFactory = ingredientConverterFactory;
            _measurementConverterFactory = measurementConverterFactory;
        }

        /// <summary>
        /// Standard conversion for measurements of the same type.  Converts the quantity from the source measurement
        /// units and returns it in the destination measurement units.
        /// </summary>
        /// <param name="srcMeasurement">Source measurement.</param>
        /// <param name="destMeasurement">Destination measuremnt.</param>
        /// <param name="quantity">Quantity to convert.</param>
        /// <returns>Converted quantity.</returns>
        public double Convert(IMeasurement srcMeasurement, IMeasurement destMeasurement, double quantity)
        {
            if (srcMeasurement.Type != destMeasurement.Type)
                throw new InvalidOperationException("Unable to perform a conversion from one measurement to another of a different Measurement Type if an ingredient is not given.");

            var srcConvert = _measurementConverterFactory.GetMeasurementConverter(srcMeasurement);
            var destConvert = _measurementConverterFactory.GetMeasurementConverter(destMeasurement);

            return destConvert.ConvertFromBaseUnits(srcConvert.ConvertToBaseUnits(quantity));
        }

        /// <summary>
        /// Complex conversion for measurements of different types (mass vs. volume).  This is made possible by
        /// using the given ingredient.  Converts the quantity from the source measurement units and returns it 
        /// in the destination measurement units.
        /// </summary>
        /// <param name="ingredient">Ingredient performing conversion for.</param>
        /// <param name="srcMeasurement">Source measurement.</param>
        /// <param name="destMeasurement">Destination measuremnt.</param>
        /// <param name="quantity">Quantity to convert.</param>
        /// <returns>Converted quantity.</returns>
        public double Convert(IIngredient ingredient, IMeasurement srcMeasurement, IMeasurement destMeasurement, double quantity)
        {
            if (srcMeasurement.Type != destMeasurement.Type)
            {
                // perform complex conversion
                var srcConvert = _measurementConverterFactory.GetMeasurementConverter(srcMeasurement);
                var destConvert = _measurementConverterFactory.GetMeasurementConverter(destMeasurement);
                var srcBaseUnits = srcConvert.ConvertToBaseUnits(quantity);
                var ingredientConverter = _ingredientConverterFactory.GetIngredientConverter(ingredient);
                var convertedBaseUnits = ingredientConverter.Convert(srcBaseUnits, srcMeasurement.Type, destMeasurement.Type);
                return destConvert.ConvertFromBaseUnits(convertedBaseUnits);
            }
            else
            {
                // perform standard conversion
                return Convert(srcMeasurement, destMeasurement, quantity);
            }
        }
    }
}
