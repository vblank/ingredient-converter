﻿using IngredientConverter.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IngredientConverter.Business.Converters
{
    /// <summary>
    /// IngredientConverter factory.
    /// </summary>
    public class MeasurementConverterFactory : IMeasurementConverterFactory
    {
        /// <summary>
        /// Encapsulate the logic for creating an Measurement converter.  Allows for a single point of change
        /// in the future if a different MeasurementConverter is desired.
        /// </summary>
        /// <param name="measurement">Measurement type of the converter.</param>
        /// <returns>A new MeasurementConverter.</returns>
        public IMeasurementConverter GetMeasurementConverter(IMeasurement measurement)
        {
            if (measurement != null)
                return new MeasurementConverter(measurement);

            return null;
        }
    }
}
