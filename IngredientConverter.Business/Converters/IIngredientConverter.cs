﻿using IngredientConverter.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IngredientConverter.Business.Converters
{
    /// <summary>
    /// Interface representing an ingredient convert.
    /// </summary>
    public interface IIngredientConverter
    {
        /// <summary>
        /// Converts the base units of the ingredient for the source measurement type to the
        /// base units of the ingredient for the destination measurement type.
        /// </summary>
        /// <param name="baseUnits">Quantity of base units to convert.</param>
        /// <param name="srcType">Source measurement type.</param>
        /// <param name="destType">Destination measurement type.</param>
        /// <returns>The converted base units of the destination measurement type.</returns>
        double Convert(double baseUnits, MeasurementType srcType, MeasurementType destType);
    }
}
