﻿using IngredientConverter.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IngredientConverter.Business.Converters
{
    /// <summary>
    /// Represents and IngredientConverter for a specific ingredient.
    /// </summary>
    public class IngredientConverter : IIngredientConverter
    {
        private IIngredient _ingredient;

        /// <summary>
        /// Constructor for the ingredient converter.
        /// </summary>
        /// <param name="ingredient">Ingredient to perform conversions with.</param>
        public IngredientConverter(IIngredient ingredient)
        {
            _ingredient = ingredient;
        }

        /// <summary>
        /// Converts the base units of the ingredient for the source measurement type to the
        /// base units of the ingredient for the destination measurement type.
        /// </summary>
        /// <param name="baseUnits">Quantity of base units to convert.</param>
        /// <param name="srcType">Source measurement type.</param>
        /// <param name="destType">Destination measurement type.</param>
        /// <returns>The converted base units of the destination measurement type.</returns>
        public double Convert(double baseUnits, MeasurementType srcType, MeasurementType destType)
        {
            return baseUnits * (_ingredient.GetEqualBaseUnits(destType) / _ingredient.GetEqualBaseUnits(srcType));
        }
    }
}