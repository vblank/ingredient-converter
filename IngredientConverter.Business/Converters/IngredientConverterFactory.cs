﻿using IngredientConverter.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IngredientConverter.Business.Converters
{
    /// <summary>
    /// IngredientConverter factory.
    /// </summary>
    public class IngredientConverterFactory : IIngredientConverterFactory
    {
        /// <summary>
        /// Encapsulate the logic for creating an ingredient converter.  Allows for a single point of change
        /// in the future if a different IngredientConverter is desired.
        /// </summary>
        /// <param name="ingredient">Ingredient type of the converter.</param>
        /// <returns>A new IngredientConverter.</returns>
        public IIngredientConverter GetIngredientConverter(IIngredient ingredient)
        {
            if (ingredient != null)
                return new IngredientConverter(ingredient);

            return null;
        }
    }
}
