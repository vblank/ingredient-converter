﻿using IngredientConverter.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IngredientConverter.Business.Converters
{
    /// <summary>
    /// Class for handling the conversion of measurements to and from their base values.
    /// </summary>
    public class MeasurementConverter : IMeasurementConverter
    {
        private double _baseUnitsPerUnit;

        /// <summary>
        /// Initializes the MeasurementConverter for the given Measurement.
        /// </summary>
        /// <param name="measurement">Measurement type to convert.</param>
        public MeasurementConverter(IMeasurement measurement)
        {
            _baseUnitsPerUnit = measurement.BaseUnitsPerUnit;
        }

        /// <summary>
        /// Converts the measurement quantity to its base units.  This is usually ounces.
        /// </summary>
        /// <param name="quantity">Quantity to convert.</param>
        /// <returns>The quantity converted to base units.</returns>
        public double ConvertToBaseUnits(double quantity)
        {
            return quantity * _baseUnitsPerUnit;
        }

        /// <summary>
        /// Converts the base unit quantity to the measurement type.
        /// </summary>
        /// <param name="baseUnits">Base unit quantity to convert.</param>
        /// <returns>The quantity converted to the measurement type.</returns>
        public double ConvertFromBaseUnits(double baseUnits)
        {
            return baseUnits / _baseUnitsPerUnit;
        }
    }
}