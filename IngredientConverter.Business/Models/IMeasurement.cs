﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IngredientConverter.Business.Models
{
    /// <summary>
    /// Interface for measurement objects.
    /// </summary>
    public interface IMeasurement
    {
        /// <summary>
        /// Unique identifier for the measurement.
        /// </summary>
        int ID { get; set; }

        /// <summary>
        /// Display literal for the measurement.
        /// </summary>
        string Literal { get; set; }

        /// <summary>
        /// The type of measurement.
        /// </summary>
        MeasurementType Type { get; set; }

        /// <summary>
        /// Display literal for the measurement type.
        /// </summary>
        String TypeLiteral { get; }

        /// <summary>
        /// Number of base units (ounces) that are in this measurement.
        /// </summary>
        Double BaseUnitsPerUnit { get; set; }
    }

    /// <summary>
    /// Enumeration for Measurement Types.
    /// </summary>
    public enum MeasurementType
    {
        /// <summary>
        /// Volume measurement type.
        /// </summary>
        Volume, 

        /// <summary>
        /// Mass measurement type.
        /// </summary>
        Mass
    }
}