﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IngredientConverter.Business.Models
{
    /// <summary>
    /// Interface to represent an ingredient.
    /// </summary>
    public interface IIngredient
    {
        /// <summary>
        /// Unique identifier for the ingredient.
        /// </summary>
        int ID { get; set; }

        /// <summary>
        /// Display literal for the ingredient.
        /// </summary>
        string Literal { get; set; }

        /// <summary>
        /// Category of the ingredient, used for grouping.
        /// </summary>
        string Category { get; set; }

        /// <summary>
        /// The number of mass base units (ounces) that equal the number of
        /// EqualVolumeBaseUnits for this ingredient.
        /// </summary>
        double EqualMassBaseUnits { get; set; }

        /// <summary>
        /// The number of volume base units (fluid ounces) that equal the number of
        /// EqualMassBaseUnits for this ingredient.
        /// </summary>
        double EqualVolumeBaseUnits { get; set; }

        /// <summary>
        /// Returns equivalent base units (ounces) of the ingredient for the given
        /// measurement type across all measurement types.  For example, you
        /// could fetch the base units (ounces) in mass and then fetch the
        /// base units (fluid ounces) in volume.  The returned values would
        /// be of equal amounts of the ingredient.
        /// </summary>
        /// <param name="type">Measurement type.</param>
        /// <returns>Base units to compare to other measurement types.</returns>
        double GetEqualBaseUnits(MeasurementType type);
    }
}
