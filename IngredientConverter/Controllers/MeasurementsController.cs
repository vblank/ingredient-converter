﻿using IngredientConverter.Business.Models;
using IngredientConverter.DataAccess.Models;
using IngredientConverter.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace IngredientConverter.Controllers
{
    /// <summary>
    /// Controller to manage API requests for measurements.
    /// </summary>
    [RoutePrefix("api/measurements")]
    public class MeasurementsController : ApiController
    {
        private IMeasurementRepository _repository;

        /// <summary>
        /// Constructor to initialize the controller and its repository.
        /// </summary>
        public MeasurementsController()
        {
            _repository = new MeasurementRepository();
        }

        /// <summary>
        /// Constructor to initialize the controller.  Takes the repository as parameter to allow
        /// for simpler testing and future implementation of dependency injection.
        /// </summary>
        /// <param name="repo">The MeasurementRepository</param>
        public MeasurementsController(IMeasurementRepository repo)
        {
            _repository = repo;
        }

        /// <summary>
        /// Gets all available Measurement objects.
        /// </summary>
        /// <returns>List of all Measurements.</returns>
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(List<Measurement>))]
        public IHttpActionResult GetAll()
        {
            return Ok(_repository.GetAll());
        }

        /// <summary>
        /// Gets the measurement object matching the given ID.
        /// </summary>
        /// <param name="id">The Measurement ID.</param>
        /// <returns>The matching Measurement.</returns>
        [HttpGet]
        [Route("{id}")]
        [ResponseType(typeof(Measurement))]
        public IHttpActionResult GetById(int id)
        {
            var measurement = _repository.Get(id);
            if (measurement != null)
                return Ok(measurement);

            return NotFound();
        }

        /// <summary>
        /// Gets all Measurement objects of type Mass.
        /// </summary>
        /// <returns>List of all Mass Measurements.</returns>
        [HttpGet]
        [Route("mass")]
        [ResponseType(typeof(List<Measurement>))]
        public IHttpActionResult GetMass()
        {
            return Ok(_repository.GetByType(MeasurementType.Mass));
        }

        /// <summary>
        /// Gets all Measurement objects of type Volume.
        /// </summary>
        /// <returns>List of all Volume Measurements.</returns>
        [HttpGet]
        [Route("volume")]
        [ResponseType(typeof(List<Measurement>))]
        public IHttpActionResult GetVolume()
        {
            return Ok(_repository.GetByType(MeasurementType.Volume));
        }

        /// <summary>
        /// Cleanup the repository.
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _repository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
