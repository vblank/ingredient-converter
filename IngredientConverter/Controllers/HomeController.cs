﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IngredientConverter.Controllers
{
    /// <summary>
    /// Controller for managing the home page.
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// Returns the view for the home page.
        /// </summary>
        /// <returns>Home page view.</returns>
        public ActionResult Index()
        {
            ViewBag.Title = "Ingredient Converter";
            return View();
        }
    }
}
