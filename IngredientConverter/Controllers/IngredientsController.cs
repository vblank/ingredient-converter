﻿using IngredientConverter.Business.Models;
using IngredientConverter.DataAccess.Models;
using IngredientConverter.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace IngredientConverter.Controllers
{
    /// <summary>
    /// Controller to manage API requests for ingredients.
    /// </summary>
    [RoutePrefix("api/ingredients")]
    public class IngredientsController : ApiController
    {
        private IIngredientRepository _repository;

        /// <summary>
        /// Constructor to initialize the controller and its repository.
        /// </summary>
        public IngredientsController()
        {
            _repository = new IngredientRepository();
        }

        /// <summary>
        /// Constructor to initialize the controller.  Takes the repository as parameter to allow
        /// for simpler testing and future implementation of dependency injection.
        /// </summary>
        /// <param name="repo">The IngredientRepository</param>
        public IngredientsController(IIngredientRepository repo)
        {
            _repository = repo;
        }

        /// <summary>
        /// Gets all available Ingredient objects.
        /// </summary>
        /// <returns>List of all Ingredients.</returns>
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(List<Ingredient>))]
        public IHttpActionResult GetAll()
        {
            return Ok(_repository.GetAll());
        }

        /// <summary>
        /// Gets the ingredient object matching the given ID.
        /// </summary>
        /// <param name="id">The Ingredient ID.</param>
        /// <returns>The matching Ingredient.</returns>
        [HttpGet]
        [Route("{id}")]
        [ResponseType(typeof(Ingredient))]
        public IHttpActionResult GetById(int id)
        {
            var ingredient = _repository.Get(id);
            if (ingredient != null)
                return Ok(ingredient);

            return NotFound();
        }

        /// <summary>
        /// Cleanup the repository.
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _repository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
