﻿using IngredientConverter.Business.Converters;
using IngredientConverter.Business.Models;
using IngredientConverter.DataAccess.Database;
using IngredientConverter.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace IngredientConverter.Controllers
{
    /// <summary>
    /// Controller to manage API requests for performing measurement conversions.
    /// </summary>
    [RoutePrefix("api/conversions")]
    public class ConversionsController : ApiController
    {
        private IIngredientRepository _ingredientRepository;
        private IMeasurementRepository _measurementRepository;
        private IConverter _converter;

        /// <summary>
        /// Constructor to initialize the dbContext, repositories, and converter.
        /// </summary>
        public ConversionsController()
        {
            var dbContext = new IngredientConverterContext();
            _ingredientRepository = new IngredientRepository(dbContext);
            _measurementRepository = new MeasurementRepository(dbContext);
            _converter = new Converter();
        }

        /// <summary>
        /// Constructor to initialize the controller.  Takes the repositories as parameters to allow
        /// for simpler testing and future implementation of dependency injection.
        /// </summary>
        /// <param name="ingredientRepo">The IngredientRepository.</param>
        /// <param name="measurementRepo">The MeasurementRepository.</param>
        /// <param name="converter">The Converter service.</param>
        public ConversionsController(IIngredientRepository ingredientRepo, IMeasurementRepository measurementRepo, IConverter converter) 
        {
            _ingredientRepository = ingredientRepo;
            _measurementRepository = measurementRepo;
            _converter = converter;
        }

        /// <summary>
        /// Converts the received quantity from the source Measurement units to
        /// the destination Measurement units and returns it.  The measurements must
        /// be of the same type (mass or volume).
        /// </summary>
        /// <param name="srcMeasurementId">Source Measurement ID.</param>
        /// <param name="destMeasurementId">Destination Measurement ID.</param>
        /// <param name="quantity">Quantity to convert.</param>
        /// <returns>The converted quantity.</returns>
        [Route("standard")]
        [ResponseType(typeof(double))]
        public IHttpActionResult GetStandardConversion(int srcMeasurementId, int destMeasurementId, double quantity)
        {
            var srcMeasurement = _measurementRepository.Get(srcMeasurementId);
            var destMeasurement = _measurementRepository.Get(destMeasurementId);

            if (srcMeasurement == null || destMeasurement == null)
            {
                return NotFound();
            }

            if(srcMeasurement.Type != destMeasurement.Type)
            {
                return BadRequest("In order to retrieve a standard conversion, the measurements must be of the same type.");
            }


            return Ok(_converter.Convert(srcMeasurement, destMeasurement, quantity));
        }

        /// <summary>
        /// Converts the received quantity from the source Measurement units to
        /// the destination Measurement units for the given ingredient and returns it.
        /// </summary>
        /// <param name="ingredientId">Ingredient to perform conversion on.</param>
        /// <param name="srcMeasurementId">Source measurement ID.</param>
        /// <param name="destMeasurementId">Destination measurement ID.</param>
        /// <param name="quantity">Quantity to convert.</param>
        /// <returns>The converted quantity.</returns>
        [Route("complex")]
        [ResponseType(typeof(double))]
        public IHttpActionResult GetIngredientConversion(int ingredientId, int srcMeasurementId, int destMeasurementId, double quantity)
        {
            var ingredient = _ingredientRepository.Get(ingredientId);
            var srcMeasurement = _measurementRepository.Get(srcMeasurementId);
            var destMeasurement = _measurementRepository.Get(destMeasurementId);

            if (ingredient == null || srcMeasurement == null || destMeasurement == null)
            {
                return NotFound();
            }

            return Ok(_converter.Convert(ingredient, srcMeasurement, destMeasurement, quantity));
        }

        /// <summary>
        /// Cleanup the dbContext and repositories.
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _measurementRepository.Dispose();
                _ingredientRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
