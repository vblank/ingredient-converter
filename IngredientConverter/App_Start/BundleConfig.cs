﻿using System.Web;
using System.Web.Optimization;

namespace IngredientConverter
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                      "~/scripts/angular.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/kendo").Include(
                      "~/scripts/kendo/kendo.core.min.js",
                      "~/scripts/kendo/kendo.ui.core.min.js",
                      "~/scripts/kendo/kendo.data.min.js",
                      "~/scripts/kendo/kendo.combobox.min.js",
                      "~/scripts/kendo/kendo.dropdownlist.min.js",
                      "~/scripts/kendo/kendo.numerictextbox.min.js",
                      "~/scripts/kendo/kendo.slider.min.js",
                      "~/scripts/kendo/kendo.datepicker.min.js",
                      "~/scripts/kendo/kendo.angular.min.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/kendo/css").Include(
                      "~/Content/kendo/kendo.common.min.css",
                      "~/Content/kendo/kendo.default.min.css"));
        }
    }
}
