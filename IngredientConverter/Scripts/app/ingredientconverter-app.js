﻿angular.module('IngredientConverter', [
    'ingredientconverter.models.conversion',
    'kendo.directives'
])
    .controller('IngredientConverterCtrl', function IngredientConverterCtrl(ConversionModel) {
        var ingredientConverterCtrl = this;

        ingredientConverterCtrl.uomsDataSource = new kendo.data.DataSource({
            transport: {
                read: {
                    url: "api/measurements",
                    dataType: "json"
                },
            },
            group: { field: "TypeLiteral" },
            sort: { field: "Literal", dir: "asc" }
        });

        ingredientConverterCtrl.ingredientsDataSource = new kendo.data.DataSource({
            transport: {
                read: {
                    url: "api/ingredients",
                    dataType: "json"
                },
            },
            group: { field: "Category" },
            sort: { field: "Literal", dir: "asc" }
        });

        function getConversion() {
            ConversionModel.getConversion(ingredientConverterCtrl.selectedIngredient, ingredientConverterCtrl.selectedFromUom, ingredientConverterCtrl.selectedToUom, ingredientConverterCtrl.fromQuantity)
                .then(function (result) {
                    ingredientConverterCtrl.toQuantity = result;
                },
                function (data) {
                    ingredientConverterCtrl.toQuantity = null;
                });
        }

        function getOppositeConversion() {
            ConversionModel.getConversion(ingredientConverterCtrl.selectedIngredient, ingredientConverterCtrl.selectedToUom, ingredientConverterCtrl.selectedFromUom, ingredientConverterCtrl.toQuantity)
                .then(function (result) {
                    ingredientConverterCtrl.fromQuantity = result;
                },
                function (data) {
                    ingredientConverterCtrl.fromQuantity = null;
                });
        }

        ingredientConverterCtrl.getConversion = getConversion;
        ingredientConverterCtrl.getOppositeConversion = getOppositeConversion;
    })
;