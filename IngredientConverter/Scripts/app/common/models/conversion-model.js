﻿angular.module('ingredientconverter.models.conversion', [])
    .service('ConversionModel', function ($http, $q) {
        var model = this,
            URLS = {
                FETCH_STANDARD: 'api/conversions/standard',
                FETCH_COMPLEX: 'api/conversions/complex'
            },
            uoms;

        function extract(result) {
            return result.data;
        }

        model.getConversion = function (ingredient, srcUom, destUom, quantity) {
            if (srcUom && destUom && quantity) {
                if(ingredient)
                    return $http.get(URLS.FETCH_COMPLEX, { params: { "ingredientId": ingredient, "srcMeasurementId": srcUom, "destMeasurementId": destUom, "quantity": quantity } }).then(extract);
                else
                    return $http.get(URLS.FETCH_STANDARD, { params: { "srcMeasurementId": srcUom, "destMeasurementId": destUom, "quantity": quantity } }).then(extract);
            }
            else
                return $q.when(null);
        }
    });