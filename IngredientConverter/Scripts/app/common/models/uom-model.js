﻿angular.module('ingredientconverter.models.uom', [])
    .service('UomModel', function ($http, $q) {
        var model = this,
            URLS = {
                FETCH: 'api/measurements'
            },
            uoms;

        function extract(result) {
            return result.data;
        }

        function cacheUoms(result) {
            uoms = extract(result);
            return uoms;
        }

        model.getUoms = function () {
            return (uoms) ? $q.when(uoms) : $http.get(URLS.FETCH).then(cacheUoms);
        }
    });