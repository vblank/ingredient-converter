﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using IngredientConverter.Business.Models;
using IngredientConverter.Business.Converters;

namespace IngredientConverter.Tests.Business.Converters
{
    [TestClass]
    public class MeasurementConverterTest
    {
        private static IMeasurementConverterFactory _measurementConverterFactory;
        private static IMeasurement _mockUom;

        [ClassInitialize]
        public static void Setup(TestContext context)
        {
            _measurementConverterFactory = new MeasurementConverterFactory();
            _mockUom = new MockMeasurement() { ID = 1, Literal = "MySrcUom", BaseUnitsPerUnit = 10, Type = MeasurementType.Mass };
        }

        [TestMethod]
        public void TestMeasurementConverterFactoryReturnsConverter()
        {
            IMeasurementConverter converter = _measurementConverterFactory.GetMeasurementConverter(_mockUom);
            Assert.IsNotNull(converter);
        }

        [TestMethod]
        public void TestMeasurementConverterToBaseUnits()
        {
            IMeasurementConverter converter = _measurementConverterFactory.GetMeasurementConverter(_mockUom);
            var result = converter.ConvertToBaseUnits(1);
            Assert.AreEqual<double>(10, result, "Measurement converter failed to base units.");

            result = converter.ConvertToBaseUnits(0);
            Assert.AreEqual<double>(0, result, "Measurement converter failed to base units.");

            result = converter.ConvertToBaseUnits(-1);
            Assert.AreEqual<double>(-10, result, "Measurement converter failed to base units.");

            result = converter.ConvertToBaseUnits(0.25);
            Assert.AreEqual<double>(2.5, result, "Measurement converter failed to base units.");
        }

        [TestMethod]
        public void TestMeasurementConverterFromBaseUnits()
        {
            IMeasurementConverter converter = _measurementConverterFactory.GetMeasurementConverter(_mockUom);
            var result = converter.ConvertFromBaseUnits(10);
            Assert.AreEqual<double>(1, result, "Measurement converter failed from base units.");

            result = converter.ConvertFromBaseUnits(0);
            Assert.AreEqual<double>(0, result, "Measurement converter failed from base units.");

            result = converter.ConvertFromBaseUnits(-10);
            Assert.AreEqual<double>(-1, result, "Measurement converter failed from base units.");

            result = converter.ConvertFromBaseUnits(2.5);
            Assert.AreEqual<double>(0.25, result, "Measurement converter failed from base units.");
        }

        public class MockMeasurement : IMeasurement
        {
            public int ID { get; set; }
            public string Literal { get; set; }
            public MeasurementType Type { get; set; }
            public String TypeLiteral { get { return Type == MeasurementType.Volume ? "Volume" : "Mass"; } }
            public Double BaseUnitsPerUnit { get; set; }
        }
    }
}
