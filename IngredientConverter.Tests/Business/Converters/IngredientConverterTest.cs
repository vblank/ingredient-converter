﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using IngredientConverter.Business.Models;
using IngredientConverter.Business.Converters;

namespace IngredientConverter.Tests.Business.Converters
{
    [TestClass]
    public class IngredientConverterTest
    {
        private static IIngredientConverterFactory _ingredientConverterFactory;
        private static IIngredient _mockIngredient;

        [ClassInitialize]
        public static void Setup(TestContext context)
        {
            _ingredientConverterFactory = new IngredientConverterFactory();
            _mockIngredient = new MockIngredient() { ID = 1, Literal = "MyIngredient", Category = "Mine", EqualMassBaseUnits = 2, EqualVolumeBaseUnits = 4 };
        }

        [TestMethod]
        public void TestIngredientConverterFactoryReturnsConverter()
        {
            IIngredientConverter converter = _ingredientConverterFactory.GetIngredientConverter(_mockIngredient);
            Assert.IsNotNull(converter);
        }

        [TestMethod]
        public void TestIngredientConverterSameType()
        {
            IIngredientConverter converter = _ingredientConverterFactory.GetIngredientConverter(_mockIngredient);
            var result = converter.Convert(103, MeasurementType.Mass, MeasurementType.Mass);
            Assert.AreEqual<double>(103, result, "IngredientConverter failed for measurements of same type.");

            result = converter.Convert(0.75, MeasurementType.Mass, MeasurementType.Mass);
            Assert.AreEqual<double>(0.75, result, "IngredientConverter failed for measurements of same type.");

            result = converter.Convert(0, MeasurementType.Mass, MeasurementType.Mass);
            Assert.AreEqual<double>(0, result, "IngredientConverter failed for measurements of same type.");

            result = converter.Convert(-5, MeasurementType.Mass, MeasurementType.Mass);
            Assert.AreEqual<double>(-5, result, "IngredientConverter failed for measurements of same type.");

            result = converter.Convert(103, MeasurementType.Volume, MeasurementType.Volume);
            Assert.AreEqual<double>(103, result, "IngredientConverter failed for measurements of same type.");

            result = converter.Convert(0.75, MeasurementType.Volume, MeasurementType.Volume);
            Assert.AreEqual<double>(0.75, result, "IngredientConverter failed for measurements of same type.");

            result = converter.Convert(0, MeasurementType.Volume, MeasurementType.Volume);
            Assert.AreEqual<double>(0, result, "IngredientConverter failed for measurements of same type.");

            result = converter.Convert(-5, MeasurementType.Volume, MeasurementType.Volume);
            Assert.AreEqual<double>(-5, result, "IngredientConverter failed for measurements of same type.");
        }

        [TestMethod]
        public void TestIngredientConverterDifferentType()
        {
            IIngredientConverter converter = _ingredientConverterFactory.GetIngredientConverter(_mockIngredient);
            var result = converter.Convert(103, MeasurementType.Mass, MeasurementType.Volume);
            Assert.AreEqual<double>(206, result, "IngredientConverter failed for mass to volume.");

            result = converter.Convert(0.75, MeasurementType.Mass, MeasurementType.Volume);
            Assert.AreEqual<double>(1.5, result, "IngredientConverter failed for mass to volume.");

            result = converter.Convert(0, MeasurementType.Mass, MeasurementType.Volume);
            Assert.AreEqual<double>(0, result, "IngredientConverter failed for mass to volume.");

            result = converter.Convert(-2, MeasurementType.Mass, MeasurementType.Volume);
            Assert.AreEqual<double>(-4, result, "IngredientConverter failed for mass to volume.");

            result = converter.Convert(103, MeasurementType.Volume, MeasurementType.Mass);
            Assert.AreEqual<double>(51.5, result, "IngredientConverter failed for volume to mass.");

            result = converter.Convert(0.75, MeasurementType.Volume, MeasurementType.Mass);
            Assert.AreEqual<double>(0.375, result, "IngredientConverter failed for volume to mass.");

            result = converter.Convert(0, MeasurementType.Volume, MeasurementType.Mass);
            Assert.AreEqual<double>(0, result, "IngredientConverter failed for volume to mass.");

            result = converter.Convert(-2, MeasurementType.Volume, MeasurementType.Mass);
            Assert.AreEqual<double>(-1, result, "IngredientConverter failed for volume to mass.");
        }

        public class MockIngredient : IIngredient
        {
            public int ID { get; set; }
            public string Literal { get; set; }
            public string Category { get; set; }
            public double EqualMassBaseUnits { get; set; }
            public double EqualVolumeBaseUnits { get; set; }
            public double GetEqualBaseUnits(MeasurementType type)
            {
                if (type == MeasurementType.Mass)
                    return EqualMassBaseUnits;
                else
                    return EqualVolumeBaseUnits;
            }
        }
    }
}
