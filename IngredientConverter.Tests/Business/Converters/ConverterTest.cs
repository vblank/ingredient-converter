﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using IngredientConverter.Business.Models;
using IngredientConverter.Business.Converters;

namespace IngredientConverter.Tests.Business.Converters
{
    [TestClass]
    public class ConverterTest
    {
        private static IConverter _converter;
        private static IMeasurement _cupUom;
        private static IMeasurement _gallonUom;
        private static IMeasurement _poundUom;
        private static IMeasurement _ounceMassUom;
        private static IIngredient _flour;

        [ClassInitialize]
        public static void Setup(TestContext context)
        {
            _converter = new Converter();
            _cupUom = new MockMeasurement() { ID = 1, Literal = "CupUom", BaseUnitsPerUnit = 8, Type = MeasurementType.Volume };
            _gallonUom = new MockMeasurement() { ID = 2, Literal = "GallonUom", BaseUnitsPerUnit = 128, Type = MeasurementType.Volume };
            _poundUom = new MockMeasurement() { ID = 3, Literal = "PoundUom", BaseUnitsPerUnit = 16, Type = MeasurementType.Mass };
            _ounceMassUom = new MockMeasurement() { ID = 4, Literal = "OunceUom", BaseUnitsPerUnit = 1, Type = MeasurementType.Mass };
            _flour = new MockIngredient() { ID = 1, Literal = "Flour", Category = "Flour", EqualMassBaseUnits = 100, EqualVolumeBaseUnits = 80 };
        }

        [TestMethod]
        public void TestConversionSameMeasurement()
        {
            var result = _converter.Convert(_cupUom, _cupUom, 0);
            Assert.AreEqual<double>(0, result, "Converter failed conversion for same measurement.");

            result = _converter.Convert(_poundUom, _poundUom, -5);
            Assert.AreEqual<double>(-5, result, "Converter failed conversion for same measurement.");

            result = _converter.Convert(_poundUom, _poundUom, 100.523);
            Assert.AreEqual<double>(100.523, result, "Converter failed conversion for same measurement.");

            result = _converter.Convert(_flour, _cupUom, _cupUom, 0);
            Assert.AreEqual<double>(0, result, "Converter failed conversion for same measurement.");

            result = _converter.Convert(_flour, _poundUom, _poundUom, -5);
            Assert.AreEqual<double>(-5, result, "Converter failed conversion for same measurement.");

            result = _converter.Convert(_flour, _poundUom, _poundUom, 100.523);
            Assert.AreEqual<double>(100.523, result, "Converter failed conversion for same measurement.");
        }

        [TestMethod]
        public void TestConversionSameType ()
        {
            var result = _converter.Convert(_gallonUom, _cupUom, 0);
            Assert.AreEqual<double>(0, result, "Converter failed conversion for same measurement type.");

            result = _converter.Convert(_gallonUom, _cupUom, 2);
            Assert.AreEqual<double>(32, result, "Converter failed conversion for same measurement type.");

            result = _converter.Convert(_cupUom, _gallonUom, 4);
            Assert.AreEqual<double>(0.25, result, "Converter failed conversion for same measurement type.");

            result = _converter.Convert(_cupUom, _gallonUom, -16);
            Assert.AreEqual<double>(-1, result, "Converter failed conversion for same measurement type.");

            result = _converter.Convert(_poundUom, _ounceMassUom, 0);
            Assert.AreEqual<double>(0, result, "Converter failed conversion for same measurement type.");

            result = _converter.Convert(_poundUom, _ounceMassUom, 2);
            Assert.AreEqual<double>(32, result, "Converter failed conversion for same measurement type.");

            result = _converter.Convert(_ounceMassUom, _poundUom, 4);
            Assert.AreEqual<double>(0.25, result, "Converter failed conversion for same measurement type.");

            result = _converter.Convert(_ounceMassUom, _poundUom, -16);
            Assert.AreEqual<double>(-1, result, "Converter failed conversion for same measurement type.");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void TestConversionDifferentTypeThrowsExceptionWithoutIngredient()
        {
            _converter.Convert(_cupUom, _poundUom, 32);
        }

        [TestMethod]
        public void TestConversionDifferentType()
        {
            var result = _converter.Convert(_flour, _cupUom, _poundUom, 0);
            Assert.AreEqual<double>(0, result, "Converter failed conversion from volume to mass.");

            result = _converter.Convert(_flour, _cupUom, _poundUom, 32);
            Assert.AreEqual<double>(20, result, "Converter failed conversion from volume to mass.");

            result = _converter.Convert(_flour, _cupUom, _poundUom, 103.5);
            Assert.AreEqual<double>(64.6875, result, "Converter failed conversion from volume to mass.");

            result = _converter.Convert(_flour, _poundUom, _cupUom, 0);
            Assert.AreEqual<double>(0, result, "Converter failed conversion from mass to volume.");

            result = _converter.Convert(_flour, _poundUom, _cupUom, 20);
            Assert.AreEqual<double>(32, result, "Converter failed conversion from mass to volume.");

            result = _converter.Convert(_flour, _poundUom, _cupUom, 64.6875);
            Assert.AreEqual<double>(103.5, result, "Converter failed conversion from mass to volume.");
        }

        public class MockIngredient : IIngredient
        {
            public int ID { get; set; }
            public string Literal { get; set; }
            public string Category { get; set; }
            public double EqualMassBaseUnits { get; set; }
            public double EqualVolumeBaseUnits { get; set; }
            public double GetEqualBaseUnits(MeasurementType type)
            {
                if (type == MeasurementType.Mass)
                    return EqualMassBaseUnits;
                else
                    return EqualVolumeBaseUnits;
            }
        }

        public class MockMeasurement : IMeasurement
        {
            public int ID { get; set; }
            public string Literal { get; set; }
            public MeasurementType Type { get; set; }
            public String TypeLiteral { get { return Type == MeasurementType.Volume ? "Volume" : "Mass"; } }
            public Double BaseUnitsPerUnit { get; set; }
        }
    }
}
