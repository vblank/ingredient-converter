﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using IngredientConverter.Controllers;
using System.Web.Http;
using System.Web.Http.Results;
using System.Linq;
using IngredientConverter.Business.Models;
using IngredientConverter.DataAccess.Repositories;
using System.Collections.Generic;
using IngredientConverter.DataAccess.Models;
using IngredientConverter.Business.Converters;

namespace IngredientConverter.Tests.Web.Controllers
{
    [TestClass]
    public class ConversionsControllerTest
    {
        private static ConversionsController controller;

        [ClassInitialize]
        public static void Setup(TestContext context)
        {
            IMeasurementRepository _uomRepo = new MockMeasurementRepository();
            IIngredientRepository _ingredientRepo = new MockIngredientRepository();
            _uomRepo.Add(new Measurement() { ID = 1, Literal = "CupUom", BaseUnitsPerUnit = 8, Type = MeasurementType.Volume });
            _uomRepo.Add(new Measurement() { ID = 2, Literal = "GallonUom", BaseUnitsPerUnit = 128, Type = MeasurementType.Volume });
            _uomRepo.Add(new Measurement() { ID = 3, Literal = "PoundUom", BaseUnitsPerUnit = 16, Type = MeasurementType.Mass });
            _uomRepo.Add(new Measurement() { ID = 4, Literal = "OunceUom", BaseUnitsPerUnit = 1, Type = MeasurementType.Mass });
            _ingredientRepo.Add(new Ingredient() { ID = 1, Literal = "Flour", Category = "Flour", EqualMassBaseUnits = 100, EqualVolumeBaseUnits = 80 });
            controller = new ConversionsController(_ingredientRepo, _uomRepo, new Converter());

        }

        [TestMethod]
        public void TestGetStandardConversion_Valid()
        {
            // Convert from and to same measurement
            IHttpActionResult result = controller.GetStandardConversion(1, 1, 4);
            Assert.IsInstanceOfType(result, typeof(OkNegotiatedContentResult<double>));
            OkNegotiatedContentResult<double> value = result as OkNegotiatedContentResult<double>;
            Assert.AreEqual(4, value.Content);

            // Convert to new measuremnt of same type
            result = controller.GetStandardConversion(1, 2, 16);
            Assert.IsInstanceOfType(result, typeof(OkNegotiatedContentResult<double>));
            value = result as OkNegotiatedContentResult<double>;
            Assert.AreEqual(1, value.Content);
        }

        [TestMethod]
        public void TestGetStandardConversion_Invalid()
        {
            // Source measurement not valid
            IHttpActionResult result = controller.GetStandardConversion(5, 1, 4);
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));

            // Destination measurement not valid
            result = controller.GetStandardConversion(1, 5, 16);
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));

            // Source and destination are not of the same type
            result = controller.GetStandardConversion(1, 3, 16);
            Assert.IsInstanceOfType(result, typeof(BadRequestErrorMessageResult));
        }

        [TestMethod]
        public void TestGetIngredientConversion_Valid()
        {
            // Convert from and to same measurement
            IHttpActionResult result = controller.GetIngredientConversion(1, 2, 2, 16);
            Assert.IsInstanceOfType(result, typeof(OkNegotiatedContentResult<double>));
            OkNegotiatedContentResult<double> value = result as OkNegotiatedContentResult<double>;
            Assert.AreEqual(16, value.Content);

            // Convert to new measuremnt of same type
            result = controller.GetIngredientConversion(1, 1, 2, 16);
            Assert.IsInstanceOfType(result, typeof(OkNegotiatedContentResult<double>));
            value = result as OkNegotiatedContentResult<double>;
            Assert.AreEqual(1, value.Content);

            // Convert to new measurement of different type
            result = controller.GetIngredientConversion(1, 1, 3, 32);
            Assert.IsInstanceOfType(result, typeof(OkNegotiatedContentResult<double>));
            value = result as OkNegotiatedContentResult<double>;
            Assert.AreEqual(20, value.Content);
        }

        [TestMethod]
        public void TestGetIngredientConversion_Invalid()
        {
            // Source measurement not valid
            IHttpActionResult result = controller.GetIngredientConversion(1,5,2,16);
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));

            // Destination measurement not valid
            result = controller.GetIngredientConversion(1,1,5,16);
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));

            // Ingredient measurement not valid
            result = controller.GetIngredientConversion(2,1,5,16);
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }
    }

    public class MockIngredientRepository : IIngredientRepository
    {
        private List<Ingredient> _ingredients = new List<Ingredient>();
        public List<Ingredient> GetAll() { return _ingredients; }
        public Ingredient Get(int id) { return _ingredients.Where(i => i.ID == id).FirstOrDefault(); }
        public void Add(Ingredient entity) { _ingredients.Add(entity); }
        public void Update(Ingredient entity) { }
        public void Delete(int id) { }
        public void Delete(Ingredient entity) { }
        public void SetOriginalRowVersion(Ingredient entity, byte[] rowVersion) { }
        public void SaveChanges() { }
        public void Dispose() { }
    }

    public class MockMeasurementRepository : IMeasurementRepository
    {
        private List<Measurement> _measurements = new List<Measurement>();
        public List<Measurement> GetAll() { return _measurements; }
        public Measurement Get(int id) { return _measurements.Where(i => i.ID == id).FirstOrDefault(); }
        public List<Measurement> GetByType(MeasurementType type) { return _measurements.Where(i => i.Type == type).ToList(); }
        public void Add(Measurement entity) { _measurements.Add(entity); }
        public void Update(Measurement entity) { }
        public void Delete(int id) { }
        public void Delete(Measurement entity) { }
        public void SetOriginalRowVersion(Measurement entity, byte[] rowVersion) { }
        public void SaveChanges() { }
        public void Dispose() { }
    }
}
