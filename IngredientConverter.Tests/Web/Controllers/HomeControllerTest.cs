﻿using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using IngredientConverter;
using IngredientConverter.Controllers;

namespace IngredientConverter.Tests.Web.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void Index()
        {
            HomeController controller = new HomeController();
            ViewResult result = controller.Index() as ViewResult;

            Assert.IsNotNull(result);
            Assert.AreEqual("Ingredient Converter", result.ViewBag.Title);
        }
    }
}
