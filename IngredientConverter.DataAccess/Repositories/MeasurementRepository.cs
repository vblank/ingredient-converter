﻿using IngredientConverter.Business.Models;
using IngredientConverter.DataAccess.Database;
using IngredientConverter.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IngredientConverter.DataAccess.Repositories
{
    /// <summary>
    /// Repository for CRUD operations on Measurements.
    /// </summary>
    public class MeasurementRepository : Repository<Measurement>, IMeasurementRepository
    {
        /// <summary>
        /// Constructor that takes no arguments.
        /// </summary>
        public MeasurementRepository()
        {
        }

        /// <summary>
        /// Constructor that takes the DbContext as a parameter so it can be shared
        /// with other repositories.
        /// </summary>
        /// <param name="context"></param>
        public MeasurementRepository(IngredientConverterContext context) : base(context)
		{
		}

        /// <summary>
        /// Return all Measurements with the given type.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public List<Measurement> GetByType(MeasurementType type)
        {
            return DbSet.Where(m => m.Type == type).ToList();
        }
    }
}