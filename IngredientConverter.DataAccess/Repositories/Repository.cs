﻿using IngredientConverter.DataAccess.Database;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace IngredientConverter.DataAccess.Repositories
{
    /// <summary>
    /// Repository for performing CRUD operations on entities.  All repositories
    /// should derive from this class.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Repository<T> : IRepository<T> where T : class
    {
        private bool disposed = false;
		private IngredientConverterContext context = null;

        /// <summary>
        /// The database set representing the entities in the database.
        /// </summary>
		protected DbSet<T> DbSet
		{
			get; set; 
		}

        /// <summary>
        /// Initializes the repository.
        /// </summary>
		public Repository()
		{
			context = new IngredientConverterContext();
			DbSet = context.Set<T>();
		}

        /// <summary>
        /// Initializes the repository using the given DbContext.
        /// </summary>
        /// <param name="context"></param>
		public Repository(IngredientConverterContext context)
		{
			this.context = context;
            DbSet = context.Set<T>();
		}

        /// <summary>
        /// Get all entities from the database of the current type.
        /// </summary>
        /// <returns></returns>
		public List<T> GetAll()
		{
			return DbSet.ToList();
		}

        /// <summary>
        /// Get the entity from the database with the given ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
		public T Get(int id)
		{
			return DbSet.Find(id);
		}

        /// <summary>
        /// Insert the given entity into the database.
        /// </summary>
        /// <param name="entity"></param>
		public void Add(T entity)
		{
			DbSet.Add(entity);
		}

        /// <summary>
        /// Sets the row version value for the row for optimistic locking.
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="rowVersion"></param>
        public void SetOriginalRowVersion(T entity, byte[] rowVersion)
        {
            context.Entry<T>(entity).OriginalValues["RowVersion"] = rowVersion;
        }

        /// <summary>
        /// Update changes to the given entity to the database.
        /// </summary>
        /// <param name="entity"></param>
		public virtual void Update(T entity)
		{
			context.Entry<T>(entity).State = EntityState.Modified;
		}

        /// <summary>
        /// Delete the given entity from the database.
        /// </summary>
        /// <param name="id"></param>
		public void Delete(int id)
		{
			DbSet.Remove(DbSet.Find(id));
		}
        
        /// <summary>
        /// Delete the given entity from the database.
        /// </summary>
        /// <param name="entity"></param>
        public void Delete(T entity)
        {
            DbSet.Remove(entity);
        }

        /// <summary>
        /// Commit all changes made to the database.
        /// </summary>
		public void SaveChanges()
		{
			context.SaveChanges();
		}

        /// <summary>
        /// Cleanup the DbContext.
        /// </summary>
		public void Dispose()
		{
			if(!disposed) {
				context.Dispose();
				disposed = true;
			}
		}
    }
}