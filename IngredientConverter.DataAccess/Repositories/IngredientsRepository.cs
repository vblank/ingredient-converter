﻿using IngredientConverter.DataAccess.Database;
using IngredientConverter.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IngredientConverter.DataAccess.Repositories
{
    /// <summary>
    /// Repository for CRUD operations on Ingredients.
    /// </summary>
    public class IngredientRepository : Repository<Ingredient>, IIngredientRepository
    {
        /// <summary>
        /// Constructor that takes no arguments.
        /// </summary>
        public IngredientRepository()
        {
        }

        /// <summary>
        /// Constructor that takes the DbContext as a parameter so it can be shared
        /// with other repositories.
        /// </summary>
        /// <param name="context"></param>
        public IngredientRepository(IngredientConverterContext context) : base(context)
		{
		}
    }
}