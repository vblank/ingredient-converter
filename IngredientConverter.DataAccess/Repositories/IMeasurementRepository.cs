﻿using IngredientConverter.Business.Models;
using IngredientConverter.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IngredientConverter.DataAccess.Repositories
{
    /// <summary>
    /// Repository for CRUD operations on Measurements.
    /// </summary>
    public interface IMeasurementRepository : IRepository<Measurement>
    {
        /// <summary>
        /// Get all ingredients of the given measurement type.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        List<Measurement> GetByType(MeasurementType type);
    }
}
