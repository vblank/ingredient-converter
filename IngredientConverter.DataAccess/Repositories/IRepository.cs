﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IngredientConverter.DataAccess.Repositories
{
    /// <summary>
    /// Repository for performing CRUD operations on entities.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IRepository<T> : IDisposable where T : class
    {
        /// <summary>
        /// Return all entities of this type.
        /// </summary>
        /// <returns></returns>
		List<T> GetAll();

        /// <summary>
        /// Return the entity with the given ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
		T Get(int id);

        /// <summary>
        /// Insert the given entity into the database.
        /// </summary>
        /// <param name="entity"></param>
		void Add(T entity);

        /// <summary>
        /// Sets the row version value for the row for optimistic locking.
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="rowVersion"></param>
        void SetOriginalRowVersion(T entity, byte[] rowVersion);

        /// <summary>
        /// Update changes to the given entity into the database.
        /// </summary>
        /// <param name="entity"></param>
		void Update(T entity);

        /// <summary>
        /// Delete the given entity from the database.
        /// </summary>
        /// <param name="id"></param>
		void Delete(int id);

        /// <summary>
        /// Delete the given entity from the database
        /// </summary>
        /// <param name="entity"></param>
        void Delete(T entity);

        /// <summary>
        /// Commit all database changes.
        /// </summary>
		void SaveChanges();
    }
}
