﻿using IngredientConverter.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IngredientConverter.DataAccess.Repositories
{
    /// <summary>
    /// Repository for CRUD operations on Ingredients.
    /// </summary>
    public interface IIngredientRepository : IRepository<Ingredient>
    {
    }
}
