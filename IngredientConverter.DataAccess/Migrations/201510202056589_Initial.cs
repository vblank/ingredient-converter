namespace IngredientConverter.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Ingredient",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Literal = c.String(),
                        Category = c.String(),
                        EqualMassBaseUnits = c.Double(nullable: false),
                        EqualVolumeBaseUnits = c.Double(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Measurement",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Literal = c.String(),
                        Type = c.Int(nullable: false),
                        BaseUnitsPerUnit = c.Double(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Measurement");
            DropTable("dbo.Ingredient");
        }
    }
}
