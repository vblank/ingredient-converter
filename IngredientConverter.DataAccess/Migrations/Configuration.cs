namespace IngredientConverter.DataAccess.Migrations
{
    using IngredientConverter.Business.Models;
    using IngredientConverter.DataAccess.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    public sealed class Configuration : DbMigrationsConfiguration<IngredientConverter.DataAccess.Database.IngredientConverterContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(IngredientConverter.DataAccess.Database.IngredientConverterContext context)
        {
            context.Ingredients.AddOrUpdate(i => new { i.Literal, i.Category },
                new Ingredient("All-Purpose Flour (Unsifted)", "Flour", 90.78, 50),
                new Ingredient("All-Purpose Flour (Sifted)", "Flour", 114.1, 50),
                new Ingredient("Granulated Sugar", "Sugar", 59.17, 50),
                new Ingredient("Confectioner's Sugar", "Sugar", 87.15, 50),
                new Ingredient("Butter", "Dairy", 49.41, 50),
                new Ingredient("Buttermilk", "Dairy", 46.28, 50),
                new Ingredient("Milk", "Dairy", 46.53, 50),
                new Ingredient("Egg Whites", "Eggs", 51.54, 50),
                new Ingredient("Beaten Eggs", "Eggs", 49.41, 50),
                new Ingredient("Egg Yolks", "Eggs", 42.04, 50),
                new Ingredient("Heavy Cream", "Dairy", 48.22, 50)
            );

            context.Measurements.AddOrUpdate(m => new { m.Literal, m.Type },
                new Measurement("Cup", MeasurementType.Volume, 8),
                new Measurement("Gallon", MeasurementType.Volume, 128),
                new Measurement("Liter", MeasurementType.Volume, 33.814),
                new Measurement("Milliliter", MeasurementType.Volume, 0.0338140225589),
                new Measurement("Ounce (volume)", MeasurementType.Volume, 1),
                new Measurement("Pint", MeasurementType.Volume, 16),
                new Measurement("Quart", MeasurementType.Volume, 32),
                new Measurement("Tbsp", MeasurementType.Volume, 0.5),
                new Measurement("Tsp", MeasurementType.Volume, 0.166666667),
                new Measurement("Kilogram", MeasurementType.Mass, 35.27396194958041),
                new Measurement("Gram", MeasurementType.Mass, 0.0352739619),
                new Measurement("Milligram", MeasurementType.Mass, 0.00003527396198069),
                new Measurement("Pound", MeasurementType.Mass, 16),
                new Measurement("Ounce (mass)", MeasurementType.Mass, 1)
            );
        }
    }
}
