﻿using IngredientConverter.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace IngredientConverter.DataAccess.Database
{
    /// <summary>
    /// The database context for the IngredientConverter app.
    /// </summary>
    public class IngredientConverterContext : DbContext
    {
        public IngredientConverterContext() : base("IngredientConverterContext")
        {
        }

        /// <summary>
        /// Current entities stored are measurements and ingredients.
        /// </summary>
        public DbSet<Ingredient> Ingredients { get; set; }
        public DbSet<Measurement> Measurements { get; set; }

        /// <summary>
        /// Settings for when creating the models.
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Remove pluralizing of table names.
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}