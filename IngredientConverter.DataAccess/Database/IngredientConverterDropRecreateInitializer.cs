﻿using IngredientConverter.Business.Models;
using IngredientConverter.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace IngredientConverter.DataAccess.Database
{
    /// <summary>
    /// Initializer for the database.  This is configured to be called in Web.config.  This is configured to
    /// completely drop and re-create the database every time the application is started.
    /// </summary>
    public class IngredientConverterDropRecreateInitializer : DropCreateDatabaseAlways<IngredientConverterContext>
    {
        /// <summary>
        /// Create the initial ingredients and measurements.
        /// </summary>
        /// <param name="context"></param>
        protected override void Seed(IngredientConverterContext context)
        {
            var ingredients = new List<Ingredient>
            {
                new Ingredient("All-Purpose Flour (Unsifted)", "Flour", 90.78, 50),
                new Ingredient("All-Purpose Flour (Sifted)", "Flour", 114.1, 50),
                new Ingredient("Granulated Sugar", "Sugar", 59.17, 50),
                new Ingredient("Confectioner's Sugar", "Sugar", 87.15, 50),
                new Ingredient("Butter", "Dairy", 49.41, 50),
                new Ingredient("Buttermilk", "Dairy", 46.28, 50),
                new Ingredient("Milk", "Dairy", 46.53, 50),
                new Ingredient("Egg Whites", "Eggs", 51.54, 50),
                new Ingredient("Beaten Eggs", "Eggs", 49.41, 50),
                new Ingredient("Egg Yolks", "Eggs", 42.04, 50),
                new Ingredient("Heavy Cream", "Dairy", 48.22, 50)
            };
            ingredients.ForEach(i => context.Ingredients.Add(i));
            context.SaveChanges();

            var measurements = new List<Measurement>()
            {
                new Measurement("Cup", MeasurementType.Volume, 8),
                new Measurement("Gallon", MeasurementType.Volume, 128),
                new Measurement("Liter", MeasurementType.Volume, 33.814),
                new Measurement("Milliliter", MeasurementType.Volume, 0.0338140225589),
                new Measurement("Ounce (volume)", MeasurementType.Volume, 1),
                new Measurement("Pint", MeasurementType.Volume, 16),
                new Measurement("Quart", MeasurementType.Volume, 32),
                new Measurement("Tbsp", MeasurementType.Volume, 0.5),
                new Measurement("Tsp", MeasurementType.Volume, 0.166666667),
                new Measurement("Kilogram", MeasurementType.Mass, 35.27396194958041),
                new Measurement("Gram", MeasurementType.Mass, 0.0352739619),
                new Measurement("Milligram", MeasurementType.Mass, 0.00003527396198069),
                new Measurement("Pound", MeasurementType.Mass, 16),
                new Measurement("Ounce (mass)", MeasurementType.Mass, 1)
            };

            measurements.ForEach(m => context.Measurements.Add(m));
            context.SaveChanges();

        }
    }
}