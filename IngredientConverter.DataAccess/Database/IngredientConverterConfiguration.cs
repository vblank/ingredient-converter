﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Web;

namespace IngredientConverter.DataAccess.Database
{
    /// <summary>
    /// Configurations for EntityFramework.  This is automatically picked up by EntityFramework.
    /// </summary>
    public class IngredientConverterConfiguration : DbConfiguration
    {
        public IngredientConverterConfiguration()
        {
            // Set the execution strategy for connection resiliency (retying transient errors)
            SetExecutionStrategy("System.Data.SqlClient", () => new SqlAzureExecutionStrategy());
        }
    }
}