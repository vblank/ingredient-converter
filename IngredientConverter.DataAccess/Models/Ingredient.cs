﻿using IngredientConverter.Business.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IngredientConverter.DataAccess.Models
{
    /// <summary>
    /// Represents an ingredient.
    /// </summary>
    public class Ingredient : IIngredient
    {
        public Ingredient()
        {
        }

        /// <summary>
        /// Constructor for an Ingredient.
        /// </summary>
        /// <param name="literal">Display literal.</param>
        /// <param name="category">Ingredient category.</param>
        /// <param name="equalVolumeUnits">Volume base units that equal the given mass base units.</param>
        /// <param name="equalMassUnits">Mass base units that equal the given volume base units.</param>
        public Ingredient(string literal, string category, double equalVolumeUnits, double equalMassUnits)
        {
            Literal = literal;
            Category = category;
            EqualVolumeBaseUnits = equalVolumeUnits;
            EqualMassBaseUnits = equalMassUnits;

        }

        /// <summary>
        /// Unique identifier for the ingredient.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Display literal for the ingredient.
        /// </summary>
        public string Literal { get; set; }

        /// <summary>
        /// Category of the ingredient.
        /// </summary>
        public string Category { get; set; }

        /// <summary>
        /// The number of mass base units (ounces) that equal the number of
        /// EqualVolumeBaseUnits for this ingredient.
        /// </summary>
        public double EqualMassBaseUnits { get; set; }

        /// <summary>
        /// The number of volume base units (fluid ounces) that equal the number of
        /// EqualMassBaseUnits for this ingredient.
        /// </summary>
        public double EqualVolumeBaseUnits { get; set; }

        /// <summary>
        /// Returns equivalent base units (ounces) of the ingredient for the given
        /// measurement type across all measurement types.  For example, you
        /// could fetch the base units (ounces) in mass and then fetch the
        /// base units (fluid ounces) in volume.  The returned values would
        /// be of equal amounts of the ingredient.
        /// </summary>
        /// <param name="type">Measurement type.</param>
        /// <returns>Base units to compare to other measurement types.</returns>
        public double GetEqualBaseUnits(MeasurementType type)
        {
            if (type == MeasurementType.Mass)
                return EqualMassBaseUnits;
            else
                return EqualVolumeBaseUnits;
        }

        /// <summary>
        /// Timestamp used for optimistic locking.
        /// </summary>
        [Timestamp]
        public byte[] RowVersion { get; set; }
    }
}