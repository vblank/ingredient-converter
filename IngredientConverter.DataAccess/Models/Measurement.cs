﻿using IngredientConverter.Business.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IngredientConverter.DataAccess.Models
{
    /// <summary>
    /// Measurement object that represents a unit of measure.
    /// </summary>
    public class Measurement : IMeasurement
    {
        public Measurement()
        {
        }

        /// <summary>
        /// Constructor for measurement objects.
        /// </summary>
        /// <param name="literal">Display literal.</param>
        /// <param name="type">Type of measurement.</param>
        /// <param name="baseUnitsPerUnit">Number of base units (ounces) are in this measurement.</param>
        public Measurement(string literal, MeasurementType type, double baseUnitsPerUnit)
        {
            Literal = literal;
            Type = type;
            BaseUnitsPerUnit = baseUnitsPerUnit;
        }

        /// <summary>
        /// Constructor for measurement objects.
        /// </summary>
        /// <param name="id">Unique identifier.</param>
        /// <param name="literal">Display literal.</param>
        /// <param name="type">Type of measurement.</param>
        /// <param name="baseUnitsPerUnit">Number of base units (ounces) are in this measurement.</param>
        public Measurement(int id, string literal, MeasurementType type, double baseUnitsPerUnit)
        {
            ID = id;
            Literal = literal;
            Type = type;
            BaseUnitsPerUnit = baseUnitsPerUnit;
        }

        /// <summary>
        /// Unique identifier for the measurement.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Display literal for the measurement.
        /// </summary>
        public string Literal { get; set; }

        /// <summary>
        /// Measurement type of the measurement.
        /// </summary>
        public MeasurementType Type { get; set; }

        /// <summary>
        /// Return literal for the measurement type.
        /// </summary>
        [NotMapped]
        public string TypeLiteral
        {
            get
            {
                return Type == MeasurementType.Volume ? "Volume" : "Mass";
            }
        }

        /// <summary>
        /// Number of base units (ounces) that are in this measurement.
        /// </summary>
        public Double BaseUnitsPerUnit { get; set; }

        /// <summary>
        /// Timestamp used for optimistic locking.
        /// </summary>
        [Timestamp]
        public byte[] RowVersion { get; set; }
    }
}